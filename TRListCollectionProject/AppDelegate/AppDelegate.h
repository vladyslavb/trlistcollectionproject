//
//  AppDelegate.h
//  TRListCollectionProject
//
//  Created by Vladyslav Bedro on 12/11/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

